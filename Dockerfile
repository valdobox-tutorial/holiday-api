FROM node:12-alpine

RUN mkdir /home/node/app
WORKDIR /home/node/app
COPY . .
ARG NODE_ENV=development
RUN npm install --no-audit --no-optional

CMD ["node", "index.js"]