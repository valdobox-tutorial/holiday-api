
const ENV = process.env

module.exports = {
  server: null,
  mockMemoryDB: null,
  basePATH: ENV.basePATH || '/holiday-api',
  db: {
    connectionString: ENV.MONGODB_CONNECTION_STRING,
    options: {
      useNewUrlParser: true, useFindAndModify: false
    }
  }
}
