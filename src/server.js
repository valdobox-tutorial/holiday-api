const express = require('express')
const swagger = require('swagger-express-middleware')

const app = express()
const router = express.Router()

require('express-async-errors')
const errorMiddleware = require('../src/middleware/error-middleware')

const path = require('path')
const swaggerSpec = path.join(__dirname, '../swagger.yaml')
const { basePATH } = require('./config')

const {
  mockMemoryDB
} = require('../test/fixtures/mock-fixtures')

const {
  mockResourceNotFound,
  mockPostConflict
} = require('swagger-express-mock-not-found-conflict')

const { middleware: auth } = require('keycloak-connect-roles').init()

module.exports = new Promise((resolve, reject) => {
  swagger(swaggerSpec, app, (err, middleware) => {
    if (err) {
      reject(err)
    }

    app.use(auth)

    app.use(
      middleware.metadata(),
      middleware.CORS(),
      middleware.files({
        apiPath: `${basePATH}/swagger`
      }),
      middleware.parseRequest(),
      middleware.validateRequest(),
      mockResourceNotFound(mockMemoryDB),
      mockPostConflict(mockMemoryDB, 'id'),
      middleware.mock(mockMemoryDB)
    )

    app.use(basePATH, router)
    app.use(errorMiddleware)

    resolve(app)
  })
})
