const swagger = require('swagger-express-middleware')
const MemoryDataStore = swagger.MemoryDataStore
const Resource = swagger.Resource

const mockMemoryDB = new MemoryDataStore()

const holidaysFixtures = [{
  id: 'testId1',
  name: 'Holiday 1',
  address: 'addres 1',
  country: 'country 1',
  adultPrice: 200,
  childPrice: 150,
  description: 'Some description',
  minAmountPerson: 1,
  maxAmountPerson: 20
}, {
  id: 'testId2',
  name: 'Holiday 2',
  address: 'addres 2',
  country: 'country 2',
  adultPrice: 300,
  childPrice: 100,
  description: 'Some description 2',
  minAmountPerson: 1,
  maxAmountPerson: 20
}]

mockMemoryDB.save(
  new Resource('/holiday-api/holidays/testId1', holidaysFixtures[0]),
  new Resource('/holiday-api/holidays/testId2', holidaysFixtures[1])
)

module.exports = {
  mockMemoryDB
}
